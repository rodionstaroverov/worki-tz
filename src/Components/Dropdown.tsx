import React, { useRef, useEffect, useState, KeyboardEvent } from 'react';
import classNames from 'classnames';
import styles from './Dropdown.module.css';
import { DropdownI } from '../interfaces';
import { useOnClickOutside } from '../lib/hooks/useOnClickOutside';

const Dropdown = ({
  defaultInputValue,
  placeholder,
  list,
  onChange,
  selectItem,
  isLoading,
  setDefaultInputValue,
}: DropdownI) => {
  const [inputValue, setInputValue] = useState<string>('');
  const [showItems, setShowItems] = useState<boolean>(false);
  const inputValueIsEmpty = inputValue.trim().length === 0;
  const ref = useRef<HTMLInputElement>(null);
  const wrapper = useRef<HTMLInputElement>(null);
  const [currentIndex, setCurrentIndex] = useState<number | null>(null);

  useOnClickOutside(wrapper, () => {
    setShowItems(false);
    setCurrentIndex(null);
  });

  useEffect(() => {
    onChange(inputValue);
    setShowItems(!inputValueIsEmpty);

    if (inputValueIsEmpty) {
      setCurrentIndex(null);
    }
  }, [inputValue]);

  useEffect(() => {
    if (ref?.current) {
      ref.current.focus();
    }
  }, []);

  const focusItem = (e: KeyboardEvent<HTMLInputElement>): void => {
    if (list.length > 0) {
      if (e.key === 'ArrowUp') {
        e.preventDefault();

        if (currentIndex === 0) {
          setCurrentIndex(list.length - 1);
        } else {
          setCurrentIndex(currentIndex === null ? list.length - 1 : currentIndex - 1);
        }
      }

      if (e.key === 'ArrowDown') {
        e.preventDefault();

        if (currentIndex === list.length - 1) {
          setCurrentIndex(0);
        } else {
          setCurrentIndex(currentIndex === null ? 0 : currentIndex + 1);
        }
      }
    }

    if (e.key === 'Escape') {
      setShowItems(false);
      setCurrentIndex(null);
      ref?.current?.blur();
    }

    if (e.key === 'Enter') {
      selectItem(list[currentIndex!]?.value);
      setShowItems(false);
      setCurrentIndex(null);
    }
  };

  useEffect(() => {
    if (list[currentIndex!]?.title) {
      setDefaultInputValue(list[currentIndex!]?.title);
    }
  }, [currentIndex]);

  const handleSelectItem = (value: string) => {
    setDefaultInputValue(list.find((item) => item.value === value)!.title);
    setShowItems(false);
    setCurrentIndex(null);
    selectItem(value);
  };

  return (
    <div ref={wrapper} className={styles.wrapper}>
      <div>
        <input
          onKeyDown={focusItem}
          onFocus={() => !inputValueIsEmpty && setShowItems(true)}
          onChange={({ target }) => setInputValue(target.value)}
          ref={ref}
          className={classNames(styles.input, { [styles.focusedInput]: showItems })}
          type="text"
          placeholder={placeholder}
          value={defaultInputValue || inputValue}
        />
      </div>

      {showItems && (
        <div className={styles.items}>
          {!isLoading &&
            list.map((item, index) => (
              <div
                onClick={() => handleSelectItem(item.value)}
                className={classNames(styles.item, { [styles.activeItem]: index === currentIndex })}
                key={item.value}
              >
                {item.title}
              </div>
            ))}

          {isLoading && (
            <div className={classNames(styles.item, styles.tombstone)}>
              <div className={styles.tombstoneLoader} />
            </div>
          )}

          {list.length === 0 && !isLoading && (
            <div className={classNames(styles.item, styles.tombstone)}>Ничего не найдено</div>
          )}
        </div>
      )}
    </div>
  );
};

export default Dropdown;
