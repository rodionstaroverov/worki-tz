import React, { useState } from 'react';
import styles from './App.module.css';
import Dropdown from './Components/Dropdown';
import { DropdownItemI } from './interfaces';
import { fetchItems } from './lib/fetchItems';
import _ from 'lodash';

const App = () => {
  const [list, setList] = useState<DropdownItemI[] | []>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [activeItem, setActiveItem] = useState<DropdownItemI | null>(null);

  const updateItems = (value: string) => {
    setActiveItem(null);
    setIsLoading(true);

    fetchItems(value)?.then((updatedList: DropdownItemI[]) => {
      setList(updatedList);
      setIsLoading(false);
    });
  };

  return (
    <div className={styles.app}>
      <h1 className={styles.title}>Выбор города</h1>

      <Dropdown
        defaultInputValue={activeItem?.title || null}
        setDefaultInputValue={(value) =>
          setActiveItem(list.find((item: DropdownItemI) => item.value === value)!)
        }
        list={list}
        onChange={_.throttle(updateItems, 200)}
        selectItem={alert}
        isLoading={isLoading}
        placeholder="Введите город"
      />
    </div>
  );
};

export default App;
