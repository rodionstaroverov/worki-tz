export interface DropdownItemI {
  value: string;
  title: string;
}

export interface DropdownI {
  list: DropdownItemI[];
  defaultInputValue: string | null;
  onChange: (value: string) => void;
  selectItem: (value: string) => void;
  setDefaultInputValue: (value: string) => void;
  isLoading: boolean;
  placeholder: string;
}
