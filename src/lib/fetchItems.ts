import { cities } from '../mock';
import { DropdownItemI } from '../interfaces';

const DELAY_TIME = 500;
let timeout: number | null = null;

const sleep = (ms: number): PromiseLike<DropdownItemI[]> => {
  if (timeout) {
    clearTimeout(timeout);
  }

  return new Promise((resolve) => {
    timeout = setTimeout(resolve, ms);
  });
};

export const fetchItems = async (value: string) => {
  await sleep(DELAY_TIME);

  const items = await cities
    .filter((item) => value.trim() !== '' && item.value.toLowerCase().match(value.toLowerCase()))
    .slice(0, 4);

  return items;
};
