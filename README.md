# Dropdown component
Select a city from the list of 15 the most popular cities in Russia

- Москва
- Санкт-Петербург
- Новосибирск
- Екатеринбург
- Нижний Новгород
- Казань
- Челябинск
- Омск
- Самара
- Ростов-на-Дону
- Уфа
- Красноярск
- Пермь
- Воронеж
- Волгоград

## Setup

Development: `yarn && yarn start`

Production: `yarn && yarn build`
